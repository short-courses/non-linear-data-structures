import binarytree as bt 
from binarytree import build 

from binarytree import build, bst, heap


# Create a balanced binary tree
balanced_tree = build([1, 2, 2, 3, 4, 4, 3])
print('Balanced Binary Tree:')
print(balanced_tree)

# Check properties of balanced tree
is_balanced = balanced_tree.is_balanced
is_symmetric = balanced_tree.is_symmetric
height = balanced_tree.height
print('\nIs this a balanced tree?', is_balanced)
print('Is this a symmetrical tree?', is_symmetric)
print('Height of the balanced tree:', height)



# Create a BST
bst_tree = bst(height=3)
print('BST (Strict):')
print(bst_tree)

# Check if this is a BST
def is_bst_strict(node):
    if node is None:
        return True

    if node.left is None and node.right is None:
        return True

    if node.left is None or node.right is None:
        return False

    return is_bst_strict(node.left) and is_bst_strict(node.right)

is_strict_bst = is_bst_strict(bst_tree)
print('\nIs this BST?', bst_tree.is_bst)
print('Is the BST strict?', is_strict_bst)



# Create min and max heap trees
min_heap_tree = heap(height=3, is_max=False)
max_heap_tree = heap(height=3, is_max=True)
print('\nMin Heap:')
print(min_heap_tree)
print('\nMax Heap:')
print(max_heap_tree)



# Create a complete binary tree
complete_tree = build([1, 2, 3, 4, 5, 6, 7])
print('\nComplete Binary Tree:')
print(complete_tree)

# Check properties of complete tree
is_complete = complete_tree.is_complete
leaf_count = complete_tree.leaf_count
levels = complete_tree.levels
print('\nIs this a complete tree?', is_complete)
print('The leaf count is:', leaf_count)
print('The levels of the tree:', levels)