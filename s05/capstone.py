import matplotlib.pyplot as plt

student_list = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
student_grade = [60, 80, 95, 85, 89, 97]

def continue_to_menu():
    print(input('\nPress ENTER to continue'))
    main_menu()

def main_menu():
    menu = """
    [program begins]
    Menu:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Lookup a student's grade
    5. Find student with highest grade
    6. Find student with lowest grade
    7. Generate graph
    8. Quit
    """
    print(menu)
    choice = input("Enter your choice: ")

    if choice == '1':
        add_student_grade()
    elif choice == '2':
        print_students()
    elif choice == '3':
        print_grades()
    elif choice == '4':
        lookup_grade()
    elif choice == '5':
        find_highest_grade()
    elif choice == '6':
        find_lowest_grade()
    elif choice == '7':
        generate_graph()
    elif choice == '8':
        exit()
    else:
        print("Invalid choice. Please try again.")
        continue_to_menu()

def add_student_grade():
    name = input("Enter the student's name: ")
    grade = int(input("Enter the student's grade: "))
    student_list.append(name)
    student_grade.append(grade)
    print(f"Added {name} with grade {grade} to the gradebook.")
    continue_to_menu()

def print_students():
    print("List of students:")
    for student in student_list:
        print(student)
    continue_to_menu()

def print_grades():
    print("List of grades:")
    for grade in student_grade:
        print(grade)
    continue_to_menu()

def lookup_grade():
    name = input("Enter the student's name to lookup: ")
    if name in student_list:
        index = student_list.index(name)
        print(f"Grade for {name}: {student_grade[index]}")
    else:
        print(f"{name} is not in the gradebook.")
    continue_to_menu()

def find_highest_grade():
    max_grade = max(student_grade)
    index = student_grade.index(max_grade)
    print(f"Student with the highest grade: {student_list[index]} ({max_grade})")
    continue_to_menu()

def find_lowest_grade():
    min_grade = min(student_grade)
    index = student_grade.index(min_grade)
    print(f"Student with the lowest grade: {student_list[index]} ({min_grade})")
    continue_to_menu()

def generate_graph():
    categories = ['60-70', '71-80', '81-90', '91-95', '96-100']
    grade_counts = [0, 0, 0, 0, 0]

    for grade in student_grade:
        if 60 <= grade <= 70:
            grade_counts[0] += 1
        elif 71 <= grade <= 80:
            grade_counts[1] += 1
        elif 81 <= grade <= 90:
            grade_counts[2] += 1
        elif 91 <= grade <= 95:
            grade_counts[3] += 1
        elif 96 <= grade <= 100:
            grade_counts[4] += 1

    plt.pie(grade_counts, labels=categories, autopct='%1.1f%%')
    plt.title("Grade Distribution")
    plt.show()
    continue_to_menu()
           
main_menu()
