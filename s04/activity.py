artist = {
	"artist_name": "Suga",
	"company": "BigHit Entertainment",
	"age": 30
}

band = {
	"band_name": "BigHit Entertainment",
	"years_active": 10,
	"hit_songs": ['Dynamite', 'Mic Drop', 'Fire']
	"is_active": True
}

# Merging the two dictionaries
profile = {}  # Create an empty dictionary for the merged profile
profile.update(artist)  # Add artist's profile to the merged dictionary
profile.update(band)    # Add band's profile to the merged dictionary

# Print the merged profile
print("Merged Profile:")
print(profile)

student_info = {
	"class": {
		"student": {
			"name": "Joon",
			"marks": {
				"physics": 85,
				"history": 90
			}
		}
	}
}

history_grade = student_info["class"]["student"]["marks"]["history"]
print("History Grade:", history_grade)

personal_info = {
	"name": "Jean",
	"age": 31,
	"salary": 45000,
	"city": "Seoul"
}

# Using dict() method to create a new dictionary
new_dict_method = dict(name=personal_info["name"], salary=personal_info["salary"])
print("New Dictionary (Using dict() method):")
print(new_dict_method)

# Using a for loop and update() method to create a new dictionary
new_dict_loop = {}
for key in ("name", "salary"):
    new_dict_loop.update({key: personal_info[key]})
print("New Dictionary (Using for loop and update() method):")
print(new_dict_loop)


employees = {
	'emp1': {"full_name" : "Amy Santiago", "salary" : 45000},
	'emp2' : {'full_name': 'Charles Boyle', 'salary': 50000},
	'emp3': {'full_name': "Rosa Diaz", "salary": 400000}
	'emp4': {'full_name': 'Jake Peralta', 'salary': 45000}
}

# Change the salary of Jake Peralta to 55000
employees['emp4']['salary'] = 55000

print("Updated Employees Dictionary:")
print(employees)


exam_data = {
	'name': ['Dwight', 'Michael', 'Jim', 'Pam', 'Andy'],
	'score': [12.5, 5, 10, 16.5, 9],
	'attempts': [1, 3, 2, 1, 3],
	'qualify':[]
}