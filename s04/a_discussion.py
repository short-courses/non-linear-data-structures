# [SECTION] Review: Dictionaries
employee_info = {
	"name": "Rhett McLaughlin",
	"level": "Co-Founder",
	"is_regular": True 
}

information = {
	"name": "Link Neal",
	"age": 44,
	"is_a_youtuber": True
}

print(information)
print('')

# Creating a dictionary using dict() function
company_info = dict(
	{
		"company_name": "Mythical Entertainment",
		"nature": "Media",
		"is_big_company": True
	}
)

print(company_info)
print('')

# Nested Dictionary
companies = {
	"company_1": company_info,
	"company_2": {
		"company_name": "Umbrella Corporation",
		"nature": "Zombie Creation",
		"is_big_company": True
	}, 
	"company_3": {
		"company_name": "Big Hit Entertainment",
		"nature": "Music",
		"is_big_company": False
	}
}

print(companies)
print('')


# [SECTION] Dictionary Methods
# get() - returns the value of the element based on its key
print(employee_info.get("name"))
print('')

# items() - returns a view object that displays a list of dictionary's tuple pairs
print(employee_info.items())
print('')

# keys() - returns a list of keys from a dictionary
print(employee_info.keys())
print('')

# values() - returns a list of all values from a dictionary
print(employee_info.values())
print('')


# [SECTION] Hash Tables with Dictionaries
grocery_prices = {
	"Onion": 600,
	"Corned Beef": 55,
	"Banana": 90,
	"Pork": 250
}

print(grocery_prices["Banana"])
print('')

for item in grocery_prices.items():
	print(item[0])
	print(item[1])

# Updating the values
grocery_prices["Onion"] = 20
grocery_prices.pop("Pork")

print('')
print(grocery_prices)


# [SECTION] Data Frame
import pandas as pd

game_stats = {
	"player_1": [72, 2, 9, 5],
	"player_2": [65, 4, 12, 7]
}

# Using the DataFrame function from pandas turns the dictionary into a data frame
data_frame = pd.DataFrame(game_stats)
print(data_frame)

# Mofifying the indexes in the data frame
data_frame = pd.DataFrame(game_stats, index=["Level", "Kills", "Deaths", "Assists"])
print(data_frame)
print('')

# We can get data from a csv file using the pandas library
data_frame = pd.read_csv("insert file location here")
print(data_frame)