#npm install heapq
from heapq import heapify, heappush, heappop, heapreplace

# [SECTION] Min-Heap and Min-Heap Operations
# Create an empty list
min_heap = []

# Convert the list into a min heap
heapify(min_heap)

# For printing the min heap
def print_min_heap():
	print("Min-heap elements: ")

	for i in min_heap:
		print(i, end=' ')
	print('\n')

# For pushing new values into the heap
heappush(min_heap, 10)
heappush(min_heap, 30)
heappush(min_heap, 20)
heappush(min_heap, 400)

print_min_heap()

# heapreplace method - replaces the smallest value in the heap
heapreplace(min_heap, 35)
print_min_heap()


# [SECTION] Max-Heap and Max-Heap Operations
max_heap = []

heapify(max_heap)

# The heapify function applies the behavior of min-heap by default. In order to change the behavior into a max-heap, we have to multiply each new value to -1.
heappush(max_heap, -1 * 10)
heappush(max_heap, -1 * 30)
heappush(max_heap, -1 * 400)
heappush(max_heap, -1 * 700)
heappush(max_heap, -1 * 20)

def print_max_heap():
	print("Max-heap elements: ")
	for i in max_heap:
		print((-1 * i), end=' ')
	print('\n')

print_max_heap()

# For removing the highest value in the max-heap
element = heappop(max_heap) # Removes the 700 from the heap
print('The element popped from the max-heap is ' + str(-1 * element))

# For replacing the highest value in the max-heap
heapreplace(max_heap, -1 * 45) # Replaces the 400 with 45
print_max_heap()