import heapq
from heapq import heapify, heappush, heappop, heapreplace

# 2. Create a Min-Heap with three levels in a tree and print the values.
min_heap = [100, 51, 41, 13, 40, 25, 36]
heapq.heapify(min_heap)
print("Min-Heap Values:")
print(min_heap)

# 3. Print the three largest values using the `nlargest` method.
largest_values = heapq.nlargest(3, min_heap)
print("\nThree Largest Values:")
print(largest_values)

# 4. Delete the smallest item from the Min-Heap and insert a new item.
# a. Show the deleted item.
deleted_item = heapq.heappop(min_heap)
print("\nDeleted Item:", deleted_item)

# b. Print the updated Min-Heap.
new_item = 0  # Insert a new item
heapq.heappush(min_heap, new_item)
print("Updated Min-Heap:")
print(min_heap)

# 5. Create a Max-Heap with three levels in a tree and print the values.
max_heap = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
heapq._heapify_max(max_heap)  # Convert to a Max-Heap
print("\nMax-Heap Values:")
print(max_heap)

# 6. Get the two largest and three smallest items from the Max-Heap.
largest_items = heapq.nlargest(2, max_heap)
smallest_items = heapq.nsmallest(3, max_heap)
print("\nTwo Largest Items:", largest_items)
print("Three Smallest Items:", smallest_items)

# 7. Sort the elements in the Max-Heap in ascending order.
sorted_max_heap = sorted(max_heap)
print("\nSorted Max-Heap (Ascending Order):")
print(sorted_max_heap)
